#!/bin/bash
rm -rf _site
rm -rf ./_assets_pass1

. ./trap_print

curl -sS -o _includes/service_template.yml https://downloads.odagrun.com/latest/files/service_template.yml
curl -sS -o _includes/install_odagrun.yml https://downloads.odagrun.com/latest/files/install_odagrun.yml
curl -sS -o _includes/uninstall_template.yml https://downloads.odagrun.com/latest/files/uninstall_template.yml
 
cp purgecss/purgecss.json ./
 export JEKYLL_ENV=PRODUCTION 
 jekyll build  --config _config_pass1.yml --destination _site_pass1 -t
 echo "PURGECSS"
 echo "*********"
 mv _site_pass1/purgecss.json _site_pass1/purgecss.js
 cat _site_pass1/purgecss.js
 echo
 echo "****"
 ls -la _site_pass1/assets/*.css
 purgecss --config _site_pass1/purgecss.js --out _site_pass1/assets
 echo purge Done
 echo "*****"
 ls -la _site_pass1/assets/*.css
 echo "*****"
 mv -vf _site_pass1/assets ./_assets_pass1
 destination=_site
 echo
 echo "final pass"
 echo "*****************"
 jekyll build  --config _config_pass2.yml
 rm -rf _site_pass1
 rm -rf _assets_pass1
