---
layout: post
title:  "Welcome to odagrun"
date:   2018-09-22 09:41:09
author: Danny Goossen
description: | 
   Copy docker images without docker, daemonless, non priviledged, nonroot on okd from GitLab-CI, update DockerHub API to set private or public, long and short description, and update MicroBadger with build in commands.
---

## oc-runner becomes ODAGRUN

**odagrun** is alive, still a bit of work to do to rename `oc-runner` to `odagrun`


## a quick teaser: Release an Image To DockerHub:

- transfer an image from ImageStream to DockerHub,
- update Dockerhub Long and short decription
- and update MircroBadger:

{% highlight yaml %}

release-image:docker:
  stage: production
  dependencies: []
  script:
    - |-
        registry_push
        --form_ISR
        --from_name=test-$CI_PIPELINE_ID
        --image=$DOCKER_NAMESPACE/$ODAGRUN_IMAGE_REFNAME:$CI_COMMIT_TAG
        --skip_label
    - |-
        registry_tag_image
        --image=$DOCKER_NAMESPACE/$ODAGRUN_IMAGE_REFNAME:$CI_COMMIT_TAG
        --tag=latest
    - |-
        DockerHub_set_description
        --image=$DOCKER_NAMESPACE/$ODAGRUN_IMAGE_REFNAME
    - |-
        MicroBadger_Update
        --allow-fail
        --image=$DOCKER_NAMESPACE/$ODAGRUN_IMAGE_REFNAME
  only:
   - tags
  except:
    - branches
  tags:
   - oc-runner-shared

{% endhighlight %}

