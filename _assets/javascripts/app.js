//= require vendor/jquery
//= require vendor/popper
//= require vendor/bootstrap
//= require vendor/typeahead

$(function() {

    // $('.collapse').collapse('hide');
    $('.list-group-item.active').parent().parent('.collapse').collapse('show');

    $('.list-group-item').click(function(){
    $(this).addClass("active").parent().siblings('.list-group-item').addClass("disabled");
});

    var pages = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('title'),
        // datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,

        prefetch: baseurl + '/search.json'
    });

    $('#search-box').typeahead({
        minLength: 0,
        highlight: true
    }, {
        name: 'pages',
        display: 'title',
        source: pages
    });

    $('#search-box').bind('typeahead:select', function(ev, suggestion) {
        window.location.href = suggestion.url;
    });


    // Markdown plain out to bootstrap style
    $('#markdown-content-container table').addClass('table');
    $('#markdown-content-container img').addClass('img-responsive');

    $(".social_share_link").click(function(event) {
      event.preventDefault();
      var share_link = $(this).prop('href');
      window.open(share_link, "social_share", "width=500,height=500");
     });

});

