---
title: Overview Odagrun Features
permalink: /docs/overview-odagrun-features/
description: |
               Overview odagrun features and build-in commands, odagrun a specific conceptual GitLab-CI runner for the RedHat Openshift Cluster.
---

- Can run on an restricted openshift cluster.

- can run and spawn multiple executors unprivileged on Openshift, with predefined Pod-sizes while respecting the cluster resources.

- can fully function with limited, restricted and free resources e.g.: Openshift-Online-Starter

- speedup artifacts downloads by using Parallel downloads with remote Gitlab-server and/or remote build openshift cluster with high latency network connections.

- dynamic choice of dependencies with a variable `dependencies` to limit downloads to the required artifacts.  

- Use of `CI_TOKEN` for pulling images from same `GitLab server` for the dispatcher and the registry commands.

- all CI functions integrated in the executable, Meaning the image does not need to contain linux-coreutils nor git for an executer to run a Job!

- integrated commands for registry operations/image creation:
    - `registry_push`
        - create from rootfs an image and push to registry
        - create from an rootfs archive an image and push to registry
        - merge a rootfs FROM an existing image and a new image to the registry.
        - add a config file
        - all non root and no Docker Daemon is involved!
        - support for schemaVersion 1 and 2
    - `registry_tag_image` Remote tag an image reference

    - No files are created for registry commands, `rootfs` and `archive` are streamed to registry with "HTTP PATCH", while calculating the content and blob SHA256SUM, and transfering a FROM image happens with up to 6 parallel threads in chunks of 4MB, again no files are saved on the file system, allowing to create a base CentOS image on `openshift-online-starter`.

- integrated ImageStream commands:
    - `ImageStream_delete_tag` delete a tag on the integrated `openshift` registry.
    - `ImageStream_delete_image` delete an image on the integrated `openshift` registry.

- integrated [DockerHub](https://hub.docker.com/) commands
    - `DockerHub_set_description` on DockerHub for a given image:
        - Short Description
        - Full Description
        - Is_Private: set repository private
    - `DockerHub_delete_tag`
    - `DockerHub_delete_repository`: delete a DockerHub repository

- integrated command `PostHook` creates a POST request for a given url.

- integrated `copy` command:
    - copy file to file/var/terminal
    - copy text to file/variable/terminal
    - option `--subtitute`: and substitute all `$xxx` or `${yyy}` environment variables
    - option `--append` : merge file/variables

- integrated `MicroBadger_Update`: update [MicroBadger](https://www.microbadger.com)
