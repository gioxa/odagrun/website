---
title: QUAY Client API to create, set description and/or visibility, delete a tag or image.
permalink: /docs/odagrun-quay-client-api-command-reference/
description: |
            odagrun QUAY API allows setting Description and/or privacy setting, Delete an Image or an Image-Tag on quay.io form Gitlab-CI.
---
 
<br>

[![quay]({%asset quay-horizontal-color.svg @optim @path %})](https://quay.io/)

## Purpose

Managing [QUAY](https://quay.io) Meta data and visibility settings from GitLab-CI, allows uniform and version controlled Meta Data without manual interactions. Besides Meta Data, the API can also delete Tags and images allowing cleanup intermediate Tags for dynamic GitLab-CI environments. 

### Common options for QUAY client API

<hr>

- `--image=quay.io/namespace/image` need to be a valid QUAY image:
    - `quay.io/gioxa/odagrun`

<br>
<div class="alert alert-info"><strong>Note:</strong> the tag for <code>--image</code> is ignored.<br>e.g.: <code>quay.io/gioxa/odagrun:latest</code> is valid too.</div>
<br>

- `--credentials=$QUAY_CREDENTIALS`
  

According to : [How to use Red Hat Quay API with examples](https://access.redhat.com/articles/3544031), thanks to CoreOS Support for providing an extract:

```
All calls to the Red Hat Quay REST API must occur via a token created for a defined Application. A new application can be created under an Organization in the Applications tab.

Generating a Token on on behalf of the currently logged in user
An access token can be generated simply by clicking on the Generate Token tab under the application, choosing scopes, and then clicking the Generate Access Token button. After conducting the OAuth flow for the current account, the newly generated token will be displayed.
```

More info: [QUAY api](https://docs.quay.io/api/)


 - Create a **base64** string credentials with a given `username=$oauthtoken` and an `oauthtoken`:

```bash
echo -e -n "$oauthtoken:<oauthtoken>" | base64 -
```
 - verify this and it should return `$oauthtoken:<oauthtoken>` with:

```bash
echo -e -n "dXNlcm5hbW....." | base64 -D  -
```

<br>
<div class="alert alert-info"><strong>Note:</strong>if <code>--credentials</code> is defined, it will priority over the environment variables .</div>

Alternatively add a secret variable to the Gitlab-CI settings:
```
QUAY_CREDENTIALS=ey...........
```
or
```
QUAY_OAUTH_TOKEN=<oauthtoken>
```
<br>
<div class="alert alert-info"><strong>Note:</strong> if <code>--credentials</code> is not defined, if <code>QUAY_OAUTH_TOKEN</code> is defined, it will take priority over <code>QUAY_CREDENTIALS</code> environment variable.</div>

- optional [`--allow-fail`] will generate only a `Warning` in stead of failing the build.


### Set OR Create QUAY registry META data


```bash
QUAY_set_description \
  --image=${repo-name} \
  [--credentials=$(base64($oauthtoken:<authtoken>))] \
  [--set-private[=yes|no|auto|none]] \
  [--description="my description"] \
  [--allow-fail]
```

*Defaults:* with priority as listed

| Option | values |
|-------------:|:----------------------------------------|
|credentials| `QUAY_OATH_TOKEN`<br>`QUAY_CREDENTIALS` |
|description|`ODAGRUN_IMAGE_DESCRIPTION`<br>`file:./description.md` <br>`file:./README.md`<br>|
|set-private| auto |


`QUAY_set_description` also allows to set the repository private:

| set-private | result |
|------:|---------------------------------|
| auto | set to **private** if Gitlab Project Visibility is `private` or `internal`<br> set to **public** if Gitlab Project Visibility is `public` |
| yes <br>`no options` | the QUAY repository is set to **private**. <br> same as `--set-private` without options |
| no |  the QUAY repository is set **public**. |
| none | the QUAY repository private setting is **not altered**! |
| !`yes`<br>!`no`<br>!`none` | defaults to **auto** |


<br>

<div class="alert alert-info"><strong>Info!</strong><br>If the repository for the given image does not exists, it will be created, this allows to create a private repository before a registry_push, ensuring that a private image stays private!</div>

<br>

<div class="alert alert-warning"><strong>Warning!</strong> <br>The Project visibility of Gitlab is only available since Gitlab version <code>10.3</code> in CI, before <code>v10.3</code>, the <code>--set-private=auto</code> option will result in a public docker Repository.</div>

<br>

### Delete a QUAY image Tag


```bash
QUAY_delete_tag \
  [--allow-fail] \
  --image=${image} \
  --reference=${tag} \
  [--credentials=$(base64($oauthtoken:<authtoken>))]
```

Deletes a given tag for a given repository image.

<br>

### Delete a QUAY repository


```bash
QUAY_delete_repository \
  [--allow-fail] \
  --image=${image} \
  [--credentials=$(base64($oauthtoken:<authtoken>))]
```

Deletes a repository with given image name.

<div class="alert alert-danger"><strong>Danger!</strong> This deletes the repository with all tags and description on the QUAY registry!</div>

<br>


