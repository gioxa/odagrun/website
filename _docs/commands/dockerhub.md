---
title: odagrun DockerHub API
permalink: /docs/odagrun-dockerhub-api-command-reference/
description: |
            odagrun DockerHub API allows setting Description and/or privacy setting, Delete an Image or an Image-Tag on DockerHub form Gitlab-CI.
---
 
<br>

[![DockerHub]({%asset dockerhub.png @optim @path %})](https://hub.docker.com/)

## Purpose

Managing DockerHub Meta data and privacy settings from GitLab-CI, allows uniform and version controlled Meta Data without manual interactions. Besides Meta Data, the API can also delete Tags and docker Repository allowing cleanup intermediate Tags. 

### Common options for DockerHub API

<hr>

- `--image=namespace/image` need to be a valid docker image `repo-name` e.g.:
    - `centos`
    - `library/centos`
    - `registry.hub.docker.com/library/centos`

<div class="alert alert-info">
    <strong>Note:</strong> the tag for <code>--image</code> is ignored.<br>e.g.: <code>centos:latest</code> is valid too.</div>

- `--credentials=$DOCKER_CREDENTIALS`
  

    - Create base64 string credentials with a docker given `username` and `password`:
    ```bash
echo -e -n "myname:mypwd" | base64 -
```
    - verify this and it should return `myname:mypwd` with:
    ```bash
echo -e -n "dXNlcm5hbWU6cGFzc3dvcmQ=" | base64 -D  -
```

    - Alternatively, on a linux computer, extract the `DOCKER_CREDENTIALS` from `/root/.docker/config.json` and take the `auth` value from `index.docker.io` **after** a succesfull `sudo docker login <reponame>`:
    
    ```json
{
    "auths": {
        "https://index.docker.io/v1/": {
            "auth": "ZGdxxxxxxxxxxxxxxxxxxUhdw=="
        },
		"quay.io": {
			"auth": "ZGdxxxxxxxxxxxxxxxxZGVsLg=="
		},
        ..........
    }
}
```

<br>

- optional [`--allow-fail`] will generate only a `Warning` in stead of failing the build.


<br>

### Set OR Create Docker Hub META data

<hr>

```yaml
deploy:
  tag: odagrun
  script:
  - >
      DockerHub_set_description
      --image=${repo-name}
      [--credentials=$(base64(username:password))]
      [--set-private[=yes|no|auto|none]]
      [--full_description="my DockerHub long description"]
      [--description="my description"]
      [--allow-fail]
```

*Defaults:* with priority as listed

| Option | values |
|- ------|--------|
|credentials| `DOCKER_CREDENTIALS` |
|full_<br>description|`ODAGRUN_IMAGE_DESCRIPTION`<br>`file:./description.md` <br>`file:./README.md`<br>|
|description|`ODAGRUN_IMAGE_TITLE`<br>`ODAGRUN_IMAGE_REFNAME`<br>`ODAGRUN_REVERSE_PROJECT_PATH`|
|set-private| auto |

<hr>

`DockerHub_set_description` also allows to set the repository private:

| set-private | |
|---|---|
| auto | set to **private** if GitLab Project Visibility is `private` or `internal`<br> set to **public** if GitLab Project Visibility is `public` |
| yes <br> or `no options` | the Docker repository is set to **private**. <br> Also `--set-private` without options|
| no |  the Docker repository is set **public**. |
| none | the Docker repository private setting is **not altered**! |
| !`yes`<br>!`no`<br>!`none` | defaults to **auto** |


<div class="alert alert-info">
    <strong>Info!</strong> If the repository for the given image does not exists, it will be created, this allows to create a private repository before a registry_push, ensuring that a private image stays private!
 </div>

 <div class="alert alert-warning">
    <strong>Warning!</strong> The Project visibility of GitLab is only available since GitLab version `10.3` in CI, before `v10.3`, the `--set-private=auto` option will result in a public docker Repository.
</div>


<br>

### Delete a Docker image Tag

<hr>


```yaml
clean:
  tag: odagrun
  script:
  - >
      DockerHub_delete_tag
      [--allow-fail]
      --image=${image}
      --reference=${tag}
      [--credentials=$DOCKER_CREDENTIALS]
```
Deletes a given tag for a given repository image.

<br>

### Delete a Docker repository

<hr>

```yaml
clean:
  tag: odagrun
  script:
  - >
      DockerHub_delete_repository
      [--allow-fail]
      --image=${image}
      [--credentials=$(base64(username:password))]
```

Deletes a repository with given image name.

<div class="alert alert-danger">
    <strong>Danger!</strong> This deletes the repository with all tags and description!
</div>

<br>

