---
title: odagrun MicroBadger API
permalink: /docs/odagrun-microbadger-api-command-reference/
description: |
               odagrun integrated MicroBadger API will automatically authorize, get a webhook and trigger an update with latest version on the DockerHub from GitLab-CI.

---

<br>

[![MicroBadger]({%asset microbadger.png @optim @path %})](https://microbadger.com/)


## Purpose

<hr>

Update [Micro Badger](https://microbadger.com/) with latest Tag on the [DockerHub](https://hub.docker.com/) for a given image.

<br>

## Usage

<hr>

```yaml
deploy:
  tag: odagrun
  script:
  - >
     MicroBadger_Update
     [--allow-fail]
     --image=repo-name
```

<div class="alert alert-warning">
    <strong>Alert! </strong>  <code> --image=repo-name</code> need to represent a valid docker image!</div>

 *valid repo-names, e.g.:*

- `centos`
- `library/centos[:whatevertag]`
- `register.hub.docker.com/library/centos`

<div class="alert alert-info">
    <strong>Info:</strong> The image tag is stripped off and completly ignored.</div>

<br>
