---
title: odagrun ImageStream API
permalink: /docs/odagrun-imagestream-api-command-reference/
description: |
               Delete images or image tags on the local RedHat openshift registry from the GiLab-CI pipeline, allowing to cleanup intermediate docker images.
---

<br>

## purpose:

<hr>
Delete an imageStream or an imageStream tag on the local openshift registry from the GiLab-CI pipeline, allowing to cleanup intermediate imageStreams or Tags.


## ImageStream_Delete_Image

<hr>
Delete an image on the integrated registry on an Openshift Cluster.

```yaml
clean:
  tag: odagrun
  script:
  - >
     ImageStream_Delete_Image
     [--name=subname]
```

<div class="alert alert-info">
    <strong>Info!</strong> Not all images can be deleted, only those which starts as : 
<br>
<code>
${OKD_NAMESPACE}/is-$CI_PROJECT_PATH_SLUG[-subname]
</code>

 </div>

<br>

## ImageStream_Delete_Tag

<hr>


```yaml
clean:
  tag: odagrun
  script:
  - >
     ImageStream_Delete_Image
     [--name=name]
     --reference=tag
```

Delete an ImageStream tag, with the same restriction as with ImageStream_Delete_Image.

<br>
