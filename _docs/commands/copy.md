---
title: odagrun copy command
permalink: /docs/odagrun-copy-command-reference/
description: |
              odagrun copy allows conditional variable and file manipulations with optional subtitute for GitLab-CI without the need of linux core utils in the build image.
---

<br>

## purpose

<hr>
**odagrun** copy command allows to copy, substitute and/or append data from a file or text to a variable,a file or the terminal output, elimination the use of a build-image with linux coreutils, while still been able to have some basic data manipulations, and some more.

## Usage:


```yaml
prepare:
  tag: odagrun
  script:
  - >
      copy
      (--from_text="text"|--from_file=file)
      (--to_var=varname|--to_file=filename|--to_term)
      [--substitute]
      [--append]
      [--quiet]
      [--if_value="${var}"]
      [--if_zero="${var}"]
```

<div class="alert alert-warning">
    <strong>Alert! </strong> when using <code>--from_text</code>, enclose option in double quotes <code>"</code>, especially when an variable consist out of multiple words.</div>

<br>

### Copy Options:

<hr>


| Options | Description |
|-----------------------:|-------------------------------------------|
| [substitute](#substitute) | all variables in the text or file are resolved.|
| append | the `from_xxx` will be appended to the defined output, exept for the `--to_term` |
| quiet | no info will be outputed, and with `--to_term` only the data is displayed. |
| if_value=<br>"${var}"| command will only execute on the `var` not empty, simple substitution is supported.|
| if_zero=<br>"${var}" | command will only be executed if `var` is not defined or empty |

<div class="alert alert-warning">
    <strong>Alert! </strong> when using <code>--from_text</code>, enclose option in double quotes <code>"</code>, especially when an variable consist out of multiple words.</div>

<br>

### substitute

<hr>

While looking through the official nginx Dockerfile, I was suprissed that one would install `gettext` to merely use the `envsubst` command, a small c-program(20k) to substitute environment variables in a file, well we no longer need to waste resources on that, meet `--substitude` as an integrated option for the **odagrun copy** function.

extract form [nginx Dockerfile](https://github.com/digitalsparky/jekyll-minifier/blob/master/lib/jekyll-minifier.rb):

```bash
    # Bring in gettext so we can get `envsubst`, then throw
	# the rest away. To do this, we need to install `gettext`
	# then move `envsubst` out of the way so `gettext` can
	# be deleted completely, then move `envsubst` back.
```

Using copy with `--substitute` will substitude all `${var}` or `$var` with the environment variables from the source text.

If an variable is not defined, the original text `${var}` or `$var` will be used and thus not substituted, while if the `var` is empty, the result will be an empty string.

Simple Variable-Substitution is posible as per [wordexp()](https://www.gnu.org/software/libc/manual/html_node/Variable-Substitution.html#Variable-Substitution) from `glibc-2.17`, and escaping can be done with a double `$` :

e.g.: `$${var}` will result in `${var}` without substitution of the variable.

<div class="alert alert-info">
    <strong>Good Practice:</strong> Always use <code>${var}</code> for substitution in stead of <code>$var</code>.</div>

<br>

**example copy with substitute:**

- with `text.txt.in` containing:

```
hallo this is  project: ${CI_PROJECT_NAME}, 
an example for substitute.

or no substitute: $${CI_PROJECT_NAME}

and the string length of:
- $$CI_PROJECT_NAME is ${#CI_PROJECT_NAME}
```

- with

```bash
CI_PROJECT_NAME="test_substitute"
```

- and with `.GitLab-ci.yaml`:

```yaml
build:
  sript:
  - >
     copy 
     --from_file=text.txt.in
     --to_file=text.txt
     --substitute
  artifacts:
    paths: 
    - text.txt
```

Results in an artifacts file `text.txt` containing:

```
hallo this is  project: test_substitute, 
an example for substitute.

or no substitute: ${CI_PROJECT_NAME}

and the string length of:
- $CI_PROJECT_NAME is 15
```

<br>

