---
title: Setup odagrun on okd
permalink: /docs/setup-odagrun-okd/
description: "Setup odagrun, a custom GitLab-runner on okd RedHat openshift cluster with automated GitLab-CI runner registration."
---



Before we can start the installation, we need some preparation:

1. an openshift *service account* with a *role binding* for that service account
2. a *runner registration token*.

<br>

## 1. Service account and rolebinding

<br>

### A. Create service account from the openshift web-console:


![openshift web console]({% asset console.png @optim @path %})


Click Import YAML/JSON, and paste the `service_template.yml`:

<details>
<summary><h4>Expand to view the service_template.yml</h4></summary>
<div markdown="1">

```yaml 

{% include service_template.yml %}

```

</div>

</details>


- and paste the content into: 

    ![import section]({% asset import_yaml.png @optim @path %})

- Press create and this will popup a message:

    ![alert permissions]({% asset permisions.png @optim @path %})

Indicating that we'll be adding a service account with edit role, needed to create our executer pods and to be able to write images to the ImageStream.

Press `Create anyway`, this is mandatory, without the install of odagrun will fail later on. 

<br>

#### B. use the Openshift CLI

Open a shell or Bash terminal and download the template in the current directory:

**notes** 

1. Before using below **oc**-command, *copy the login command* from the web console and paste it into your terminal.
2. replace odagrun with project u want to use if not odagrun in `oc project odagrun`
3. if no project exists, `oc new-project odagrun`

```bash
oc project odagrun
oc create sa sa-odagrun
oc policy add-role-to-user edit -z sa-odagrun
```

*REMARK:* if on `oc policy add-role-to-user` you get :`Error from server (Forbidden): policybindings ":default" is forbidden: User "xxxx" cannot get policybindings in the namespace "odagrun": no RBAC policy matched`, update your openshift client to match the server version.

<br>

## 2. The runner registration token

<hr>

refer to the [registering-a-shared-runner](https://docs.GitLab.com/ce/ci/runners/#registering-a-shared-runner),



<details>
<summary><h4>Expand to to read more on the different types of runners from the GitLab Docs</h4></summary>
<br>
<div markdown="1">

A Runner can be specific to a certain project or serve any project
in GitLab CI. A Runner that serves all projects is called a shared Runner.

<br>

### Shared, specific and group Runners

You can either register it as shared or specific. runner.
You can only register a shared Runner if you have admin access to
the GitLab instance. The main differences between a shared and a specific Runner
are: 

[sourced from GitLab Docs:](https://docs.GitLab.com/ce/ci/runners/)

- **Shared Runners** are useful for jobs that have similar requirements,
  between multiple projects. Rather than having multiple Runners idling for
  many projects, you can have a single or a small number of Runners that handle
  multiple projects. This makes it easier to maintain and update them.
  Shared Runners process jobs using a [fair usage queue](https://docs.GitLab.com/ce/ci/runners/#how-shared-runners-pick-jobs).
  In contrast to specific Runners that use a FIFO queue, this prevents
  cases where projects create hundreds of jobs which can lead to eating all
  available shared Runners resources.
- **Specific Runners** are useful for jobs that have special requirements or for
  projects with a specific demand. If a job has certain requirements, you can set
  up the specific Runner with this in mind, while not having to do this for all
  Runners. For example, if you want to deploy a certain project, you can setup
  a specific Runner to have the right credentials for this. The [usage of tags](https://docs.GitLab.com/ce/ci/runners/#using-tags)
  may be useful in this case. Specific Runners process jobs using a [FIFO] queue.
- **Group Runners** are useful when you have multiple projects under one group
  and would like all projects to have access to a set of Runners. Group Runners
  process jobs using a [FIFO] queue.

A Runner that is specific only runs for the specified project(s). A shared Runner
can run jobs for every project that has enabled the option **Allow shared Runners**
under **Settings > CI/CD**.

Projects with high demand of CI activity can also benefit from using specific
Runners. By having dedicated Runners you are guaranteed that the Runner is not
being held up by another project's jobs.

You can set up a specific Runner to be used by multiple projects. The difference
with a shared Runner is that you have to enable each project explicitly for
the Runner to be able to run its jobs.

Specific Runners do not get shared with forked projects automatically.
A fork does copy the CI settings (jobs, allow shared, etc) of the cloned
repository.

</div>

</details>

<br>

### a. Registering a shared Runner

You can only register a shared Runner if you are an admin of the GitLab instance.

Grab the shared-Runner token on the **admin/runners** page

![register runner]({% asset register.png @optim @path %})

Shared Runners are enabled by default as of GitLab 8.2, but can be disabled with the Disable shared Runners button which is present under each project's **Settings ➔ CI/CD page**. Previous versions of GitLab defaulted shared Runners to disabled.

<br>

### b. Registering a specific Runner

Registering a specific Runner can be done in two ways:

    1. Creating a Runner with the project registration token
    2. Converting a shared Runner into a specific Runner (one-way, admin only)

<br>

### c. Registering a specific Runner for a project

To create a specific Runner without having admin rights to the GitLab instance, visit the project you want to make the Runner work for in GitLab:

    Go to <strong>Settings > CI/CD</strong> to obtain the token

<br>

### d. Registering a group Runner

Creating a group Runner requires Maintainer permissions for the group. To create a group Runner visit the group you want to make the Runner work for in GitLab:

    Go to <strong>Settings > CI/CD</strong> to obtain the token


For more indept reading about locking and sharing runners, please refer to the [GitLab docs](https://docs.GitLab.com/ce/ci/runners/).

<br>

## 3. Install odagrun:

<br>

### A. Install odagrun from the openshift console:


![openshift web console]({% asset console.png @optim @path %})


*Click Import YAML/JSON, and paste the `install_odagrun.yml`:*

<details>
<summary><h4>Expand to view the install_odagrun.yml</h4></summary>
<br>
<div markdown="1">

```yaml	

{% include install_odagrun.yml %}

```
</div>

</details>

Or alternatively download the [install_odagrun.yml](https://downloads.odagrun.com/latest/files/install_odagrun.yml)

- and paste the content into: 

![import section]({% asset import_yaml.png @optim @path %})

- Press create and fill in the parameters for this deployment.

![parameters]({% asset parameters.png @optim @path %})

1. on can set an alternative `appName` for the application, which will cause all the resourses to be created with that name.
This is particular usefull when creating multiple runners in the same namespace and defaults to `odagrun`.

    **note:** use only smallcaps and no symbols nor spaces, except for `-`.
 
2. Set the GitLab CI URL, defaults to `https://GitLab.com`

3. Set runner registration token: absolutly mandatory!

Scroll further down to:

4. set the runner description, as displayed in GitLab runner overview.

5. set the runner tags, a comma separated tag_list

6. indicate if runner can pickup untagged jobs

7. indicate runner is locked to this project

8. if this is not a new install, indicate to overwrite or append an existing configMap with runners.


![success]({% asset succes.png @optim @path %})

Indicating everything went smooth, you can close the window now, or review the parameters while the boostrap is running.

Depending on the timing, one might still see a `pod-<appName>-virgin` when closing the window, which is the bootstrap to register a new runner and create a new deployment config `<appName>`, e.g. `odagrun` when using the default *appName*.

When the `<appName>` is shown in the overview of the project:

![deployment config]({% asset dc-odagrun.png @optim @path %})

odagrun is ready for business and poling for jobs with the given tag_list.

<br>

### B. Install with the openshift CLI:


Open a shell or Bash terminal:

**note** Before using below **oc**-command, *copy the login command* from the web console and paste it into your terminal.

Next we need to prepare the params, only `GITLAB_RUNNER_REGISTRATION_TOKEN` has no default, and makes this a mandatory parameter.

Openshift provides [multiple methodes](https://docs.openshift.com/online/dev_guide/templates.html#uploading-a-template) to get the parameters from our template merged or processed while creating:

1. From the commandline
    -  by using -p:
    ```bash
REG_TOKEN=fkdjgfodjkvogf
curl -LSs https://downloads.odagrun.com/latest/files/install_odagrun.yml | oc new-app -f - -p GITLAB_RUNNER_REGISTRATION_TOKEN=${REG_TOKEN}
```

this will register a new runner on `https://GitLab.com`, with tags `odagrun` and description `odagrun`, and will be ready to execute jobs when the deployment config is done.

2. By creating a file containing the parameters:

    - template.params
    ```bash
ODAGRUN_APPLICATION_NAME=my-new-odagrun
GITLAB_RUNNER_CI_URL=https://GitLab.com
GITLAB_RUNNER_REGISTRATION_TOKEN=fkdjgfodjkvogf
GITLAB_RUNNER_DESCRIPTION="oc runner on openshift online"
GITLAB_RUNNER_TAGS="odagrun,odagrun-starter"
```
    - And run
    ```bash
curl -LSs https://downloads.odagrun.com/latest/files/install_odagrun.yml | oc new-app -f - --param-file=template.params
```
    - this will produce the following output for a succesfull install of the template.
    
```bash
--> Deploying template "odagrun-test/odagrun" for "-" to project odagrun-test

     odagrun
     ---------
     odagrun, a GitLab runner with some bells and wistles
     

     odagrun is started as odagrun-install, when started, a new runner will be registred through the GitLab-api and a new deployment with the new runner will be started. Replacing or Upsdating this deployment with odagrun.


     * With parameters:
        * appName=my-new-odagrun
        * GitLab CI URL=https://GitLab.com
        * GitLab Runner Registration Token=fkdjgfodjkvogf
        * Runner description=oc runner on openshift online
        * Runner tags=odagrun,odagrun-starter
        * untagged=True
        * locked=False

--> Creating resources ...
    pod "pod-my-new-odagrun-virgin" created
--> Success
    Run 'oc status' to view your app.
```

in this example `pod "my-new-odagrun-install" created` starts a pod that will do the install of odagrun:

1. create an imagestream
2. register the runner with GitLab-server
3. starts a depoyment config.

and Ready to use!

<br>

## Update or Replace

<hr>

- same as for install and use:

```bash
ODAGRUN_CONFIG_REPLACE=False
```
to append a new runner, and:

```bash
ODAGRUN_CONFIG_REPLACE=True
```

To unregister any runners present and replace by this one.

<br>

## Uninstall

<hr>

Best way is to delete the project!

- If that is not an option, use the [uninstall template](https://downloads.odagrun.com/latest/files/uninstall_template.yml) in the console,

<details>
<summary><h4>Expand to view the uninstall template</h4></summary>
<br>
<div markdown="1">

```yaml

{% include uninstall_template.yml %}

```

</div>

</details>

- or with the CLI:

```bash
curl -sSL https://downloads.odagrun.com/latest/files/uninstall_template.yml | oc new-app -f - \
-p GITLAB_RUNNER_REGISTRATION_TOKEN=CONFIRM \
-p ODAGRUN_UNINSTALL=True \
-p ODAGRUN_APPLICATION_NAME=my-new-odagrun
```

- this will delete:
    - imagestream "is-my-new-odagrun" deleted
    - deploymentconfig "my-new-odagrun" deleted
    - replicationControler "my-new-odagrun-1" deleted
    - pod "my-new-odagrun-1-5k8hr" deleted
    - configmap "my-new-odagrun-configmap"
- where as the following resourses need to be deleted manualy after above `uninstall_template.yml`
    - serviceaccount "sa-my-new-odagrun" e.g. with CLI `oc delete serviceaccount sa-my-new-odagrun`
    - rolebinding "edit-my-new-odagrun" e.g. with CLI `oc delete roleBinding edit-my-new-odagrun`

## Update to New Version

Since we use the *ImageStream* trigger, openshift will regulary check for new updates of *gioxa/odagrun:latest*, and the runner will be automaticly update to the new version.

When this behaviour is unwanted, modify the template or edit the runner deployment configuration:

1. remove the section for imageChange in triggers
2. add `gioxa/odagrun:<desired version>` as *imageName* in the template section *DeploymentConfig.spec.template.spec.containers[0].imageName* config.


## Issue's with Minishift


It might that executers get stuck becaus of dns issue's with minishift for the local registry.

Work around:

Use your terminal:
```
$>minishift openshift registry
172.30.1.1:5000
```
in webconsole of Openshift, `Edit Deployment Config odagrun`, add an environmental variable:

| variable | value |
|---|---|
| ODAGRUN_REGISTRY_HOST | 172.30.1.1:5000 |

A new deployment will start and the executors will be able to start.



<br>


