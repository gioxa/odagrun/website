---
title: odagrun Dependencies Filter extension
permalink: /docs/odagrun-dependencies-filter-feature-reference/
description: |
               odagrun a conceptual GitLab-runner allows selective dependencies filtering as an extension of GitLab-CI dependencies to preserve network resources on RedHat Openshift Cluster.
---

## Purpose

Selectively retrieving artifacts, based on an variable, e.g. `$CI_JOB_NAME` to avoid downloading uneeded/unwanted artifacts for the Job at hand, reducing the waste  of resources.

## Example:

`.GitLab-ci.yml` build file:
```yaml
.build_tc: &build_tc
  stage: build_stage1
  script:
     - ./make_os "${CI_JOB_NAME#gcc-}"
  artifacts:
     expire_in: 90min
     paths:
       - tc.tar.gz

gcc-el7-x64: *build-tc
gcc-el6-i386: *build-tc
gcc-el6-x64: *build-tc
gcc-el7-arm: *build-tc
gcc-el7-arm64: *build-tc

.build_image: &build_image
  stage: build_image
  variables:
    dependencies: |
                    - "gcc-${CI_JOB_NAME#img-}"
    GIT_STRATEGY: none
  script:
    - merge_tc "${CI_JOB_NAME#img-}"

img-el7-arm: *build_image
img-el7-arm64: *build_image
img-el7-x64: *build_image
img-el6-x64: *build_image
img-el6-i386: *build_image
```

In this example we build toolchains, for different Release and/or architecture in stage 1,
where in stage 2 we want the artifact of stage 1 which correspond to the release and architecture we need as defined on the trailing part of `${CI_JOB_NAME}`.

For this example, each `gcc-xxx-xxx` creates an artifact of several hundreds of Mega Bytes, which has now been averted by the dependencies variable that filters only what we need.

Thus, only those artifacts from previous builds which match the variable substitution will be downloaded.

<div class="alert alert-info" role="alert">
        <strong>Note:</strong> 
If the job `dependencies` is set to `[]`, the variable dependencies has no function, we can only filter from what is available.</div>

<div class="alert alert-warning" role="alert">
        <strong>Warning:</strong> Only standard substitution is supported, not extended <code>wordexp()</code> substitution.</div>

[top](#oc-runner)

