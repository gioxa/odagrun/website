---
title: Git Cache
permalink: /docs/odagrun-git-cache-feature-reference/
description: |
               odagrun a conceptual GitLab-runner with GIT CACHE as a docker image layer on openshift Cluster, to easy and preserve network resources from the GitLab server.

---

## Feature: GIT_CACHE_STRATEGY

### Purpose

When building  on a remote build systems, a lot of bandwidth/time might be used to git clone/fetch for each job in the pipeline, especially on docker builds that are clean slates when started.

With gitcache, we can reduce that network usage.

To enable define a variable in the `.GitLab-ci.yml`: 

```yaml
variables:
  GIT_CACHE_STRATEGY: [pull|push-pull|pull-clean|clean]
```

- `GIT_CACHE_STRATEGY`:

    - pull: pull only
    - push-pull: pull and push when finished
    - pull-clean: pull and clean cache when done
    - push: only push
    - clean: remove the cache

- Branches:

When a cache for a branche/tag is not availleble, the master is pulled, if push was defined, push to the branch!

### Example:

```yaml
stages:
  - cache
  - build
  - test
  - clean_branch_cache

# define the global cache settings=> all pull
variables:
  GIT_CACHE_STRATEGY: pull

refresh_cache
  stage: cache
  image: scratch
  variables:
    GIT_CACHE_STRATEGY: push-pull
  script:
    - cd /


build:1:
  stage: build
  script:
    - .....

build:2:
  stage: build
  script:
    - .....

```

This will refresh the cache in stage cache and for the builds, the cache image will be merged with the requested image, so when pod starts, cache is present.

[top](#oc-runner)

