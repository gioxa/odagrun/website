---
title:  odagrun variables
permalink: /docs/odagrun-variables-feature-reference/
description: odagrun variables as helpers, Gitlab-CI variable extensions and MetaData for automatic manifest labels while creating docker images with GitLab-CI on Openshift.
---

<br>

Odagrun provides a set of variables as helpers and specific variables that can be use to feed the meta info for the OCI label scheme while pushing or transfering docker images with registry-push and with the DockerHub API.


<br>
<div class="alert alert-info"><strong>Info:</strong><br> For an overview or explanation of the standard GitLab-CI variables, please refer to the <span markdown="1">**[GitLab CI/CD Variables](https://docs.GitLab.com/ce/ci/variables/)**.</span></div>
<br>


## important variable protection:

<hr>

Odagrun uses variables to ensure that other projects cannot overwrite your images by means of `CI_PROJECT_PATH` and other variables, but since GitLab does not provide a way to garanty a variable is issued by GitLab-ci, **odagrun** protects these variable from overwriting.


the Following GitLab-CI variables cannot be overwritten by user intervention and are thus protectect readonly GitLab-CI system defined:

- `CI_PROJECT_PATH`
- `CI_PROJECT_URL`
- `GitLab_USER_NAME`
- `GitLab_USER_EMAIL`
- `GitLab_USER_ID`
- `GitLab_USER_LOGIN`
- `CI_REPOSITORY_URL`
- `CI_REGISTRY`
- `CI_REGISTRY_IMAGE`
- `CI_PROJECT_DIR`

<br>

## custom defined odagrun variables:

<hr>

### git decribe:

#### Purpose

Odagrun provides a variable `ODAGRUN_GIT_VERSION` and `ODAGRUN_GITVERSION` containing the `git decribe --always --tags`, this becomes in handy when a build image does not contain git-command.

#### example:

```bash
$> echo $ODAGRUN_GIT_VERSION
0.0.33-15-ge2b011bf
$> 
```

<br>
<div class="alert alert-info"><strong>Note:</strong> <code>ODAGRUN_GIT_VERSION</code> has no ending newline <code>\n</code>.</div>

<div class="alert alert-warning"><strong>Note:</strong> <code>ODAGRUN_GIT_VERSION</code> is not available when <code>GIT_STRATEGY: none</code> is set.</div>
<br>


### Info regarding the Build image used:

- `ODAGRUN_BUILD_IMAGE` : full image repo name
- `ODAGRUN_BUILD_IMAGE_NICK` : name used in CI
- `ODAGRUN_BUILD_IMAGE_TAG` : tag or reference of image
- `ODAGRUN_BUILD_IMAGE_SHA` : SHA256 reference


### General purpose provided variables:

##### ODAGRUN_COMMIT_TAG_SLUG:

 a tag in the form of e.g. `3-1-9`, with a given `CI_COMMIT_TAG`: `3.1.9`

##### Reverse Project Path Slug:

for naming and or registry with single level directories, odagrun provides, e.g with a given `CI_COMMIT_TAG=oc-runner/build-image` :

- **ODAGRUN_REVERSE_PROJECT_PATH_SLUG** results in `build-image-oc-runner`
- **ODAGRUN_REVERSE_PROJECT_PATH** results in `build-image oc-runner`


<br>
<div class="alert alert-warning"><strong>Note:</strong> If <code>CI_PROJECT_PATH</code> contains more then 1 slash, <code>ODAGRUN_REVERSE_PROJECT_PATH*</code> contains only the last 2 elements, whereas if the last 2 elements are equal wil result only in the last element.</div>
<br>

Examples `ODAGRUN_REVERSE_PROJECT_PATH`:


| CI_PROJECT_PATH | result |
|-----------------------------|-----------------------|    
| gioxa/oc-runner/build-image | build-image oc-runner |
| gioxa/oc-runner/oc-runner  | oc-runner |
| oc-runner/build-image | build-image oc-runner |


#### date variables:

Sample:

```bash
$> echo $ODAGRUN_RFC3339_DATE
2018-08-12T08:23:23Z
$>  echo $ODAGRUN_SHORT_DATE
20180810
$>
```

#### New-Line:

- `nl` is set to `\n` since setting a bash variable with newline is a bit tricky in yaml without the use of `echo`-command.

<br>

## Variables for autolabeling

<hr>

the odagrun internal command **registry_push** will automatic append OCI labels to the pushed image according to the [org.opencontainer.image](https://github.com/opencontainers/image-spec/blob/master/annotations.md#pre-defined-annotation-keys) labeling scheme.

### Priorities:


<br>
<div class="alert alert-primary"><strong>Priority:</strong><br>All variables which are defined in the <strong>docker_config.yml</strong> forego the set environment variables.</div>
<br>

1. docker_config.yml:

    1. `org.opencontainers.image.xxx`
    2. `xxx`
    3. additonal `maintainer` as alternative for `org.opencontainers.image.authors`
    4. additional `license` as alternative for `org.opencontainers.image.licenses`

2. environment
    - `ODAGRUN_IMAGE_XXXX`

3. exeptions: the following labels in `docker_config.yml` are **ignored**
    - `org.opencontainers.image.source`
    - `org.opencontainers.image.revision`
    - `org.opencontainers.image.ref.name`

4. `org.opencontainers.image.version`,searched in this sequential order:
    1.  file `docker_config.yml` label: `org.opencontainers.image.version`
    2.  file `docker_config.yml` label: `version`
    3.  environment var: `ODAGRUN_IMAGE_VERSION`
    3.  environment var: `ODAGRUN_GIT_VERSION`
    4.  environment var: `GIT_VERSION`
    5.  environment var: `GITVERSION`
    9.  read-file `$CI_PROJECT_DIR/.VERSION`
    6.  read-file `$CI_PROJECT_DIR/.GIT_VERSION`
    7.  read-file `$CI_PROJECT_DIR/.GITVERSION`
    8.  read-file `$CI_PROJECT_DIR/.tarball-version`


5. `org.opencontainers.image.ref.name`, label in `docker_config.yml` is **ignored**
	- namepart:
        1. environment var : `ODAGRUN_IMAGE_REFNAME`
        2. slug( environment var: `ODAGRUN_IMAGE_TITLE`)
        3. reverse slug of last 2 sections of `CI_PROJECT_PATH` e.g.:
            - `gioxa/oc-runner/image` becomes `image-oc-runner`
            - `gioxa/oc-runner/oc-runner` becomes `oc-runner`
    - version-part as per above point 4

6. `org.opencontainers.image.title` *if not defined in docker_config.yml*
    1. `ODAGRUN_IMAGE_TITLE`
	2. `ODAGRUN_IMAGE_NAME`
    3. reverse of last 2 sections of `CI_PROJECT_PATH` e.g.:
        - `gioxa/oc-runner/image` becomes `image oc-runner`
        - `gioxa/oc-runner/oc-runner` becomes `oc-runner`

7.  `org.opencontainers.image.authors` *if not defined in docker_config.yml* 
Autors is read from `$CI_PROJECT_DIR/AUTHORS`, with all `\n` removed (flattened).

<br>

## variables used for label schema

<hr>

##### 1. User Provided for `org.opencontainers.image`

| `OCI` | Variable |
| --- :| --- |
| .title | `ODAGRUN_IMAGE_TITLE` |
| .image.licenses | `ODAGRUN_IMAGE_LICENSES` |
| .image.vendor | `ODAGRUN_IMAGE_VENDOR` |
| .description | `ODAGRUN_IMAGE_DESCRIPTION` |
| .documentation | `ODAGRUN_IMAGE_DOCUMENTATION` |
| .url   | `ODAGRUN_IMAGE_URL` |

##### 2. provided by **odagrun** for `org.opencontainers.image`

**prefix=org.opencontainers.image**

| $prefix | Variable |
| --- :| --- |
| .version |    `ODAGRUN_IMAGE_VERSION` |

##### 3. provided by **GitLab-ci** for `org.opencontainers.image`


**prefix=org.opencontainers.image**

| $prefix| Variable |
| --- :| --- |
| .source |   `CI_PROJECT_URL` |
| .revision |   `CI_COMMIT_SHA` |

##### 4. provided by **odagrun** for `com.odagrun.build.image*`

| com.odagrun.build | Variable |
| --- :| --- |
| .image | `ODAGRUN_BUILD_IMAGE` |
| .image-nick | `ODAGRUN_BUILD_IMAGE_NICK` |
| .image-tag | `ODAGRUN_BUILD_IMAGE_TAG` |
| .image-digest | `ODAGRUN_BUILD_IMAGE_SHA` |

##### 5. provided by **GitLab-CI** for `com.odagrun.build.commit` 


prefix=`com.odagrun.build.commit`

| $prefix | Variable |
| ------------------ :| --- |
| .author | `GitLab_USER_NAME`, `GitLab_USER_EMAIL` |
| .id | `CI_COMMIT_SHA` |
| .message | `CI_COMMIT_MESSAGE` |
| .ref | `CI_COMMIT_REF_NAME` |
| .tag | `CI_COMMIT_TAG` |


##### 6. provided by **GitLab-CI** for `com.odagrun.build` 

| com.odagrun.build | Variable |
| ----------------:| --- |
| .environment | `CI_ENVIRONMENT_NAME` |
| .job-url |  `CI_PROJECT_URL`, `CI_JOB_ID` |


##### 7. provided by **odagrun** for `com.odagrun.build` 


| com.odagrun.build | Variable |
| ---------------:| --- |
| .version | `ODAGRUN_GIT_VERSION` |

<br>

## variables for Docker Hub
<hr>

User Provided Variables:

| Label | Variable |
| --- | --- |
| Short_description | `ODAGRUN_IMAGE_TITLE` |
| Full_description | `ODAGRUN_IMAGE_DESCRIPTION` |
| credentials | `DOCKER_CREDENTIALS` |

<br>

## Credential variables:

| Variable | target |
| --- | --- |
| `DOCKER_CREDENTIALS` | dockerHub repository and api |
| `QUAY_CREDENTIALS` | quay.io repository only |

  
### How to Create Credentials:

- Create base64 string credentials with a docker given `username` and `password`:
    ```bash
echo -e -n "myname:mypwd" | base64 -
```
- verify this and it should return `myname:mypwd` with:
    ```bash
echo -e -n "dXNlcm5hbWU6cGFzc3dvcmQ=" | base64 -D  -
```

- Alternatively, on a linux computer, extract the `DOCKER_CREDENTIALS` from `/root/.docker/config.json` and take the `auth` value from `index.docker.io` **after** a succesfull `sudo docker login <reponame>`: 
    ```json
{
    "auths": {
        "https://index.docker.io/v1/": {
            "auth": "ZGdxxxxxxxxxxxxxxxxxxUhdw=="
        },
		"quay.io": {
			"auth": "ZGdxxxxxxxxxxxxxxxxZGVsLg=="
		},
        ..........
    }
}
```

## Debugging

<hr>

Setting `ODAGRUN_DEBUG` to True will enable debug output for the runner.


<div class="alert alert-info" role="alert"><strong>Remark:</strong> If the dispatcher is run with <code>--debug</code> option, then the executer will also output debug info.</div>

<br>

