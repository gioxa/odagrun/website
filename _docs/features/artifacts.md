---
title: Odagrun Artifacts Extension of GitLab-CI
permalink: /docs/odagrun-artifacts-extensions-feature-reference/
description: | 
               odagrun GitLab-runner Artifacts Extension of GitLab-CI with wildcards and fast parallel downloads on okd RedHat Openshift cluster for fast artifacts downloads on high latency connections.
---

### Purpose

- extend GitLab-runner functionality to allow more granulated selection of artifacts files with wildcards `*` and variable substitution.
- speedup donwloading of artifacts by means of parallel downloads to increase the download speed on high latency connections.

### wildcard extension for GitLab Artifacts:


The GitLab-ci yaml linter does not allow to use wildcards and/or variable substitution in the artifacts paths, to circomvent this with odagrun we use quotes and define as e.g.:


`.GitLab-ci.yml` build file:
```yaml
build:
  stage: build
  script:
     - ....
  artifacts:
     paths:
       - '${ODAGRUN_GITVERSION}/*.tar.gz'
```

The above configuration will add all `*.tar.gz` files in directory `${ODAGRUN_GIT_VERSION}` to the JOB artifacts.

<br>

<div class="alert alert-info"><strong>Note:</strong> Always use use quotes or yaml features to fake a valid path and odagrun will glob it.</div>

<br>

#### valid notations to circomvent GitLab-ci Linter:

```yaml
paths:
  - >-
       ${ODAGRUN_GITVERSION}/*.tar.gz
  - '${ODAGRUN_GITVERSION}/*.tar.gz'
  - "${ODAGRUN_GITVERSION}/*.tar.gz"

```

### parallel downloads extension.

#### Problem.

When setting up a private Build environment, one might be confronted with high latency between the GitLab-ci server and your local build environment causing a lot of time to be spend on downloading artifacts sequentialy.

#### Solution

odagrun will download the artifacts in parallel and unzip in correct order.

![artifacts]({% asset artifacts.png @optim @path %})

odagrun has same functionality as [deployctl](www.deployctl.com) for artifacts downloading, and uses up to 6 threads with progress notification to allow fast downloading of artifacts in parallel.

<br>

<div class="alert alert-primary"><strong>Note:</strong> <br> Very interesting when connection time between build-server and GitLab-server have a high latency connection.</div>

<br>
