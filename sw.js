---
layout: null
---

const staticCacheName = "odagrun-static-{{ site.env.ODAGRUN_GITVERSION }}";

console.log("installing service worker");

const filesToCache = [
  "/",
  {% for file in site.static_files %}{% if file.extname == ".jpg"  %}'{{ site.baseurl }}{{ file.path }}',
  {% else if file.extname == ".png"  %}'{{ site.baseurl }}{{ file.path }}',
  {% else if file.extname == ".svg"  %}'{{ site.baseurl }}{{ file.path }}',{% endif %}{% endfor %}

  {% for page in site.html_pages %}
    '{{ page.url }}',
  {% endfor %}
  {% for post in site.posts %}
    '{{ post.url }}',
  {% endfor %}
    {% for section in site.data.docs %}
    {% for items in section.docs %}
     {% assign item_url = items | prepend:"/docs/" | append:"/" %}"{{ item_url }}",{% endfor %}{% endfor %}

];

self.addEventListener("install", function(e){
  self.skipWaiting();
  e.waitUntil(
    caches.open(staticCacheName).then(function(cache){
      return cache.addAll(filesToCache);
    })
  )
});

self.addEventListener("activate", function(e){
  e.waitUntil(
    caches.keys().then(function(cacheNames){
      return Promise.all(
        cacheNames.filter(function(cacheName){
          return cacheName.startsWith("odagrun-static-")
            && cacheName != staticCacheName;
        }).map(function(cacheName){
          return cache.delete(cacheName);
        })
      )
    })
  )
});

self.addEventListener("fetch", function(e){
  e.respondWith(
     caches.match(e.request).then(function(response) {
       return response || fetch(e.request);
     })
   )
});


